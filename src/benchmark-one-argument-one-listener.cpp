#include <smoke/signal.hpp>

int main()
{
	smoke::signal<void(int)> fire;
	fire += [](int i) { };

	for (int i = 0; i < 10000000; ++i)
		fire(i);
}
