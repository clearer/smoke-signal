#include <smoke/signal.hpp>


#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <doctest.h>

TEST_CASE("a lambda can recieve signals, with no arguments")
{
	auto was_called = false;
	smoke::signal<void()> fire;
	fire += [&]() { was_called = true; };
	fire();
	CHECK(was_called == true);
}


TEST_CASE("multiple lambdas can recieve signals, with no arguments")
{
	auto calls = 0;
	smoke::signal<void()> fire;
	fire += [&]() { calls++; };
	fire += [&]() { calls++; };
	fire();
	CHECK(calls == 2);
}


