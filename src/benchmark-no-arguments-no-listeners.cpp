#include <smoke/signal.hpp>

int main()
{
	smoke::signal<void()> fire;

	for (int i = 0; i < 10000000; ++i)
		fire();
}
