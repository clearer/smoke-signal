#include <smoke/signal.hpp>

int main()
{
	smoke::signal<void()> fire;
	fire += [](){};

	for (int i = 0; i < 10000000; ++i)
		fire();
}
