#include <smoke/signal.hpp>


#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <doctest.h>

TEST_CASE("a lambda can recieve signals with integer arguments")
{
	auto signal_value = 0;
	smoke::signal<void(int)> fire;
	fire += [&](int s) { signal_value += s; };
	fire(10);
	CHECK(signal_value == 10);
}


TEST_CASE("multiple lambdas can recieve signals with integer arguments")
{
	smoke::signal<void(int)> fire;
	fire += [&](int s) { CHECK(s == 10); };
	fire += [&](int s) { CHECK(s == 10); };
	fire(10);
}

