#include <smoke/signal.hpp>

int main()
{
	smoke::signal<void(int)> fire;
	for (int i = 0; i < 17; ++i) fire += [](int) { };

	for (int i = 0; i < 10000000; ++i)
		fire(i);
}
