#include <smoke/signal.hpp>


#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <doctest.h>

TEST_CASE("a lambda can recieve signals with two integer arguments")
{
	auto signal_value = 0;
	smoke::signal<void(int, int)> fire;
	fire += [&](int s, int n) { signal_value += s * n; };
	fire(10, 20);
	CHECK(signal_value == 200);
}


TEST_CASE("multiple lambdas can recieve signals with two integer arguments")
{
	auto signal_value = 0;
	smoke::signal<void(int, int)> fire;
	fire += [&](int s, int n) { signal_value += s * n; };
	fire += [&](int s, int n) { signal_value += s * n; };
	fire(10, 20);
	CHECK(signal_value == 400);
}

