#pragma once

#include <functional>
#include <vector>

namespace smoke {
template<class S>
struct signal {
	template<class _S> 
	signal<S>& operator += (_S&& f) {
		listeners.emplace_back(f);
		return *this;
	}

	template<class ...A>
	signal<S> const& operator() (A... args) const {
		auto const n = listeners.size();
		for (size_t i = 0; i < n; ++i)
			listeners[i](args...);
		return *this;
	}

private:
	std::vector<std::function<S>> listeners;
};
}
