# smoke::signal - a minimalistic pure C++, header only, signal framework

smoke::signal is standards compliant, no warnings, C++ framework for signal handling.

smoke::signal is similar to libsigc++, the main difference is the size of the library.
smoke::signal does have reduced functionality, compared to libsigc++, in that it doesn't
support disconnecting signals. This is a minor inconvenience; either rebuild
the signal emitter or disable the handlers internally.

Being able to remove event listeners require an identifier for each listener.
Using a simple vector to do so, makes it impossible, since a vector doesn't
store anything but the listener itself.

# Migration

This project has been moved to https://gitea.gigo-games.dk/frederik/smoke-signal. Any future development will happen there.

# How to use Smoke

The following demonstrates how functions, functor classes and lambdas can all
be used.

```
#include <smoke/signal.hpp>
#include <iostream>

auto function(int n) -> void
{
	std::cout << "Function got an event: " << n << '\n';
}

struct functor
{
	auto operator () (int n) -> void
	{
		std::cout << "Functor got an event: " << n << '\n';
	}
};

int main()
{
	smoke::signal<void(int)> bonfire;

	bonfire += [](int n) { std::cout << "lambda got an event: " << n << '\n'; };
	bonfire += functor();
	bonfire += function;

	bonfire(0);
}
```

# Benchmarks

The library contains a few benchmarks in `src/`. The results are uplifting when
compared to `libsigc++`, in that none of the benchmarks are slower than
equivelant benchmarks for `libsigc++` -- as soon as there are any event
listeners, `smoke::signal` is always faster.

| # of arguments | # event listeners | libsigc++ | smoke::signal |
| -------------: | ----------------: | --------: | ------------: |
| 0              |  0                | 0.01s     | 0.00s         |
| 0              |  1                | 0.27s     | 0.00s         |
| 1              |  0                | 0.03s     | 0.00s         |
| 1              |  1                | 0.27s     | 0.03s         |
| 0              | 17                | 0.52s     | 0.32s         |
| 1              | 17                | 0.52s     | 0.32s         |

Benchmarking is done by creating a single signal with either 0 or 1 integer
parameter. A set of lambdas (0, 1 or 17) with matching signatures and empty
bodies are attached to the signal and the signal is then fired 10000000 times
in a simple for loop. The benchmarks are compiled with meson's release
buildtype.

Benchmarking then includes fireing the signal, as well as creating it and
attaching the event handlers.

# Tests

Tests are implemented using [doctest](https://github.com/onqtam/doctest). Doctest is
integrated into the project as a meson subproject and should be downloaded automatically.

# Meson

smoke::signal contains a meson build file that declares a dependency. This should
make it easy to the library use in meson projects. It also support installing
the header file in a system dependent standard location.

Meson is also used to build the test programs.
