#include <sigc++/sigc++.h>

int main()
{
	sigc::signal<void(int)> signal;
	for (int i = 0; i < 17; ++i) signal.connect([](int){});
	for (int i = 0; i < 10000000; ++i)
		signal.emit(i);

}
